﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {

    public static bool gameisOver;
    public GameObject gameOverUI;

    void Start()
    {
        gameisOver = false;
    }
    void Update () {
        if (gameisOver)
            return;

       
		if(GameObject.FindGameObjectWithTag("GameMaster").GetComponent<PlayerStats>().Lives <= 0)
        {
            EndGame();
        }
	}

    void EndGame()
    {
        gameisOver = true;
        gameOverUI.SetActive(true);
    }
}
