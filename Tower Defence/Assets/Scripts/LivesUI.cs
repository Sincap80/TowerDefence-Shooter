﻿using UnityEngine;
using UnityEngine.Collections;
using UnityEngine.UI;

public class LivesUI : MonoBehaviour {

    public Text livesText;
    public GameObject gameMaster;

	void Update () {
        int lives = gameMaster.GetComponent<PlayerStats>().Lives;
        livesText.text = lives + " LIVES";
	}
}
