using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour {

    [HideInInspector] // only the speed
    public float speed;

    public float startSpeed = 10f;
    public float health = 100;
    public int moneyGain = 50;

    public GameObject deathEffect;

    //For PlayerStats Variables
    public GameObject gameMaster;

    void Start()
    {
        speed = startSpeed;
        
    }

    public void TakeDamage (float amount)
    {
        health -= amount;

        if(health <= 0)
        {
            Die();
        }
    }

    public void Slow(float slowingPct)
    {
        speed = startSpeed * (1f - slowingPct);
    }

    void Die ()
    {

        GameObject.FindGameObjectWithTag("GameMaster").GetComponent<PlayerStats>().Money += moneyGain;

        GameObject effect = (GameObject) Instantiate(deathEffect, transform.position, Quaternion.identity);
        Destroy(effect, 5f);
        
        Destroy(gameObject);
    }

}
