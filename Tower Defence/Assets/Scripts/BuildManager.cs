﻿using UnityEngine;
using UnityEngine.Networking;

public class BuildManager : NetworkBehaviour {

    public static BuildManager instance;
    public Node targetNode;
    void Awake()
    {
        if(instance != null)
        {
            Debug.LogError("More than one BuildManage in scene");
            return;
        }
        instance = this;
    }

    private TurretBlueprint turretToBuild;
    public GameObject buildEffect;
    public bool CanBuild { get { return turretToBuild != null; } }
    public bool HasMoney { get { return GetComponent<PlayerStats>().Money >= turretToBuild.cost; } }

    public void SelectTurretToBuild(TurretBlueprint turret)
    {
        turretToBuild = turret;
    }

    [Command]
    public void CmdBuildTurretOn ()
    {
        if(GetComponent<PlayerStats>().Money < turretToBuild.cost)
        {
            Debug.Log("Not enough Money");
            return;
        }

        GetComponent<PlayerStats>().Money -= turretToBuild.cost;

        GameObject turret = (GameObject) Instantiate(turretToBuild.prefab, targetNode.GetBuildPosition(), Quaternion.identity);
        targetNode.turret = turret;

        GameObject effect = (GameObject)Instantiate(buildEffect, targetNode.GetBuildPosition(), Quaternion.identity);
        Destroy(effect, 5f);

        NetworkServer.Spawn(turret);
        NetworkServer.Spawn(effect);

        Debug.Log("Turret Build! Money Left: " + GetComponent<PlayerStats>().Money);
    }


	
}
